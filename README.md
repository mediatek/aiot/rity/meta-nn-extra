# RITY-nn-extra

meta-nn-extra is a collection of layers to ease the integration of ai/ml frameworks over our rity-nn sdk. It is delevered "as-is" and is not officially supported

# Documentation

* [BSP documentation](https://mediatek.gitlab.io/aiot/rity/meta-mediatek-bsp)
* [RITY documentation](https://mediatek.gitlab.io/aiot/rity/meta-rity)
* [RITY tools documentation](https://baylibre.gitlab.io/rich-iot/tools/rity-tools)
* [RITY-nn documentation](https://mediatek.gitlab.io/aiot/rity/meta-nn)
* [RITY-nn-extra documentation](https://mediatek.gitlab.io/aiot/rity/meta-nn-extra)
