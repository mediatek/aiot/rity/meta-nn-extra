Overview
========

.. note::
   ``meta-nn-extra`` is a collection of layers to ease the integration of different ai/ml frameworks over our RITY SDK. This documentation assumes you are able to build and flash the RITY SDK as explained in the `RITY SDK manual <https://mediatek.gitlab.io/aiot/rity/meta-rity/>`_ and the `RITY-NN SDK <https://mediatek.gitlab.io/aiot/rity/meta-nn/index.html>`_.  This guide will only describe commands to add support for packages in this extra layer only

Supported frameworks
-----------------------------
.. csv-table:: Supported frameworks
        :header: "Frameworks", "Version"
        :widths: 60, 40

        "NNAPI", 1.3
        "Onnxruntime", 1.8
