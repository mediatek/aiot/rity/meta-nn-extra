Building AI/ML image with extra packages
========================================
Fetching the code
-----------------
.. parsed-literal::

   $ mkdir rity; cd rity
   $ repo init -u git@gitlab.com:mediatek/aiot/bsp/manifest.git -b |release| -m rity-nn.xml
   $ repo sync
   $ export TEMPLATECONF=${PWD}/src/meta-nn-extra/conf/
   $ source src/poky/oe-init-build-env

Configuring the image
---------------------
You should follow instructions on the official documentation to `configure the image <https://mediatek.gitlab.io/aiot/rity/meta-nn/building.html#configuring-the-image>`_.

To add packages from ``meta-nn-extra`` please look at the section of each desired packages.

Building and flashing the image
-------------------------------
The image is built with the following command (just as the default rity image):

.. prompt:: bash $

   bitbake rity-demo-image

To flash it, follow instructions in the  `RITY SDK manual flashing section <https://mediatek.gitlab.io/aiot/rity/meta-rity/getting-started/flashing.html>`_
